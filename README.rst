The Bone Machine
================

|Build Status|  |PyPI|

In the spirit of "Make the Right Things Easy", The Bone Machine is an extensible interactive shell for project skeleton generators.

Full documentation is available on `readthedocs
<http://thebonemachine.readthedocs.org/en/stable>`_.

bonemachine
***********
FIXME: write this

Installation
************

bonemachine is developed for Python 3 or later.

::

    pip install bonemachine


Getting Started
***************

FIXME: write this

Similar tooling which inspired this work
****************************************
- `garethr/puppet-module-skeleton <https://github.com/garethr/puppet-module-skeleton/>`_ -  useful extensions to the ```puppet module generate``` skeleton generator
- `yeoman/yeoman <https://github.com/yeoman/yeoman/>`_ - Javascript/Node-centric web app skeleton generator.